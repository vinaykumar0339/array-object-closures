// created keys function
function keys(obj) {
    let keys = [];

    if (typeof obj !== 'object') {
        return "please provide an object";
    }

    for (let key in obj) {
        keys.push(key);
    }
    return keys;
}

// created values function
function values(obj) {
    let values = [];

    if (typeof obj !== 'object') {
        return "please provide an object";
    }

    for (let key in obj) {
        values.push(obj[key]);
    }
    return values;
}

// created mapObject function
function mapObject(obj, cb) {

    if (typeof obj !== 'object') {
        return "please provide an object";
    }

    if(typeof cb !== 'function'){
        return "please provide a function as callback";
    }
    
    for (let key in obj) {
        let modifiedObj = cb(obj[key], key, obj);
        obj[key] = modifiedObj;
    }
    return obj;
}

// created pairs function
function pairs(obj) {
    let pairArray = [];

    if (typeof obj !== 'object') {
        return "please provide an object";
    }

    for (let key in obj) {
        pairArray.push([key, obj[key]]);
    }
    return pairArray;
}

// created invert function
function invert(obj) {
    let newObj = {}

    if (typeof obj !== 'object') {
        return "please provide an object";
    }

    for (let key in obj) {
        newObj[obj[key]] = key;
    }
    return newObj;
}

// created defaults function
function defaults(obj, defaultProps) {

    let newObj = {...obj};

    if (typeof obj !== 'object' && typeof defaultProps !== 'object') {
        return "please provide both parameters as an object";
    }

    for (let key in defaultProps) {
        if (!(key in newObj)) {
            newObj[key] = defaultProps[key];
        }
    }
    return newObj;
}


module.exports = {
    keys,
    values,
    mapObject,
    pairs,
    invert,
    defaults
}