const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testObject2 = {name: "vinay", email: "vinay9961@gmail.com"}
const {keys, values, mapObject,pairs,invert,defaults} = require('./Objects');

// calling keys function
var objKeys = keys(testObject);
console.log("keys method output:");
console.log(objKeys);

// calling values function
var objValues = values(testObject);
console.log("values method output:");
console.log(objValues);

//calling mapObject function
var mapObjects = mapObject(testObject, (val, key) => val + 5);
console.log("mapObject method output:");
console.log((mapObjects));

// calling pairs function
var pairsObj = pairs(testObject);
console.log("pairs method output:");
console.log(pairsObj);

// calling invert function
var invertObj = invert(testObject);
console.log("invert method output:");
console.log(invertObj);

// calling defaults function
var defaultObj = defaults(testObject, testObject2);
console.log("defaults method output:");
console.log(defaultObj);