const {counterFactory, limitFunctionCallCount, cacheFunction} = require('./Closures');


// calling the limitFunctionCallCount
const limitFunction = limitFunctionCallCount(() => "hello", 4);
console.log(limitFunction()); 
console.log(limitFunction()); 
console.log(limitFunction()); 
console.log(limitFunction()); 
console.log(limitFunction()); 


// calling the cacheFunc
const cacheFunc = cacheFunction((a,b) => a-b);
console.log(cacheFunc(2,3));
console.log(cacheFunc(2,2));
console.log(cacheFunc(2,2));