// created counterFactory function
function counterFactory() {
    return {
        count: 0,
        increment: function () {
            this.count++;
        },
        decrement: function() {
            this.count--;
        }
    }
}

// created limitFunctionCallCount function
function limitFunctionCallCount(cb, n) {
    let counter = 0;

    if (typeof cb !== 'function') {
        return "please provide a function as callback";
    }

    if (!(Number.isInteger(n))) {
        return "please provide second argument as an integer";
    }

    function cbInvokeFunc() {
        if (counter < n) {
            counter++
            return cb();
        }else {
            return null;
        }
    }
    return cbInvokeFunc;
}


function cacheFunction(cb) {
    let cache = {};

    if (typeof cb !== 'function') {
        return "please provide a function as callback";
    }
    
    function cbInvokeFunc() {
        let key = [...arguments].join(",")
        if (key in cache) {
            return cache[key];
        }
        cache[key] = cb(...arguments);
        return cache[key];
        
    }
    return cbInvokeFunc;
}


module.exports = {
    counterFactory,
    limitFunctionCallCount,
    cacheFunction
}
