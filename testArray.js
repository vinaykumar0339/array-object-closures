const items = [1, 2, 3, 4, 5, 5];
const nestedArray = [1, [2], [[3]], [[[4]]]];

const {each, map, reduce, find, filter, flatten} = require('./Arrays');


// calling each function
console.log("for each method output:");
each(items, (element, index) => console.log(element,index));

// calling map function
var mapArray = map(items, (element) => element * 2);
console.log("map method output:");
console.log(mapArray);

// calling reduce function
var reduceValue = reduce(2, (a,b) => a + b);
console.log("reduce method output:");
console.log(reduceValue);

// calling find function
var finding = find(items, (a) => a % 2 === 0);
console.log("find method output:");
console.log(finding);

//calling filter function
var filterArray = filter(items, (element) => element%2 === 0);
console.log("filter method output:");
console.log(filterArray);

//calling flatten function
var flattenArray = flatten(nestedArray);
console.log("flatten method output:");
console.log(flattenArray);