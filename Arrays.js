// created each function
function each(elements, cb) {

    if (!(Array.isArray(elements))) {
        return "please provide an array elements";
    }

    if(typeof cb !== 'function'){
        return "please provide a function as callback";
    }

    for (let key in elements) {
        cb(elements[key], key, elements);
    }
}

// created map function
function map(elements, cb) {
    let newArray = [];

    if (!(Array.isArray(elements))) {
        return "please provide an array elements";
    }

    if(typeof cb !== 'function'){
        return "please provide a function as callback";
    }

    for (let key in elements) {
        let modifiedValue = cb(elements[key], key, elements);
        newArray.push(modifiedValue);
    }
    return newArray;
}


// created reduce function
function reduce(elements, cb, startingValue) {
    let result = startingValue ? startingValue : elements[0];  
    
    if (!(Array.isArray(elements))) {
        return "please provide an array elements";
    }

    if(typeof cb !== 'function'){
        return "please provide a function as callback";
    }

    if (startingValue) {
        for (let i=0; i<elements.length; i++) {
            result = cb(result, elements[i], i, elements);
        }
    }else {
        for (let i=1; i<elements.length; i++) {
            result = cb(result, elements[i], i, elements);
        }
    }
    return result;
}


// created find function
function find(elements, cb) {

    if (!(Array.isArray(elements))) {
        return "please provide an array elements";
    }

    if(typeof cb !== 'function'){
        return "please provide a function as callback";
    }

    for (let key in elements) {
        if (cb(elements[key]) == true) {
            return elements[key];
        }
    }
    return undefined;
}


// created filter function
function filter(elements, cb) {
    let newArray = [];

    if (!(Array.isArray(elements))) {
        return "please provide an array elements";
    }

    if(typeof cb !== 'function'){
        return "please provide a function as callback";
    }

    for (let key in elements) {
        if (cb(elements[key]) == true) {
            newArray.push(elements[key]);
        }
    }
    return newArray;
}



// created flatten function
function flatten(elements) {
    let newArray = [];

    if (!(Array.isArray(elements))) {
        return "please provide an array elements";
    }

    for (let item of elements) {
        if (Array.isArray(item)) {
            newArray = newArray.concat(flatten(item));
        }else {
            newArray.push(item);
        }
    }

    return newArray;
}


module.exports = {
    each,
    map,
    reduce,
    find,
    filter,
    flatten
}